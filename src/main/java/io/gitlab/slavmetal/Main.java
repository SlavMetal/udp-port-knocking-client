/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/

package io.gitlab.slavmetal;

import java.net.UnknownHostException;
import java.util.Arrays;

public class Main {

    /**
     * Entry point.
     * @param args first argument - server's address, all others - ports to knock (space-delimited).
     */
    public static void main(String[] args) {
        try {
            new UDPClient(args [0], Arrays.asList(args).subList(1, args.length)).startClient();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
