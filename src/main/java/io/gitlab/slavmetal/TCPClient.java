/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/

package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Basic class to send and receive data via TCP.
 */
class TCPClient {
    private final Logger logger = LoggerFactory.getLogger(UDPClient.class);
    private InetAddress serverAddress;  // Server's address
    private int serverPort;             // Server's port

    /**
     * @param serverAddress server's address.
     * @param serverPort    server's port.
     */
    TCPClient(InetAddress serverAddress, int serverPort) {
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
    }

    /**
     * Some dummy functionality to send and receive string.
     */
    void startClient() {
        logger.info("Started TCP client");

        try (Socket socket = new Socket(serverAddress, serverPort);
             ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream input = new ObjectInputStream(socket.getInputStream()))
        {
            output.writeObject("Hello there, server!");
            String inputMessage = (String) input.readObject();
            System.out.println("Response: " + inputMessage);
        } catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

}
