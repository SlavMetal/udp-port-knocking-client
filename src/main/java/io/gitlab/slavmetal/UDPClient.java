/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/

package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.List;

/**
 * Class responsible for port-knocking and main functionality.
 */
class UDPClient {
    private final Logger logger = LoggerFactory.getLogger(UDPClient.class);

    private InetAddress serverAddress;                          // Server's address we should knock
    private List<String> portSequence;                          // Sequence of ports we should knock

    private final static int UDP_SEND_DELAY = 50;               // Delay between knocks (ms)
    private final static int UDP_WAIT_DELAY = 2000;             // Delay to wait for server's response (ms)
    private final static String KNOCK_MESSAGE = "Knock-knock";  // String sent in the knock packet

    /**
     * @param serverAddress server's address we should knock.
     * @param portSequence  sequence of ports we should knock.
     * @throws UnknownHostException if server can't be found.
     */
    UDPClient(String serverAddress, List<String> portSequence) throws UnknownHostException {
        this.serverAddress = InetAddress.getByName(serverAddress);
        this.portSequence = portSequence;
    }

    /**
     * Send knocks via UDP and start TCP client in case of right port sequence.
     */
    void startClient() {
        try (DatagramSocket udpSocket = new DatagramSocket()) {
            logger.info("Starting to send requests");
            // Knock ports with some delay
            for (String port : portSequence){
                byte[] bufferRequest = KNOCK_MESSAGE.getBytes();
                DatagramPacket udpRequest = new DatagramPacket(bufferRequest, bufferRequest.length, serverAddress, Integer.parseInt(port));
                udpSocket.send(udpRequest);
                logger.info("Sent knock to port " + port);
                Thread.sleep(UDP_SEND_DELAY);
            }

            // Wait some time for the reply
            byte[] bufferResponse = new byte[256];
            DatagramPacket udpResponse = new DatagramPacket(bufferResponse, bufferResponse.length);
            udpSocket.setSoTimeout(UDP_WAIT_DELAY);
            udpSocket.receive(udpResponse);

            // And start TCP client if we got one
            String messageResponse = new String(udpResponse.getData(), 0, udpResponse.getLength());
            logger.info("Got TCP port " + messageResponse);
            new TCPClient(serverAddress, Integer.parseInt(messageResponse)).startClient();
        } catch (SocketException e) {
            logger.error("Can't create DatagramSocket");
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            logger.error("No reply received, terminating now");
            e.printStackTrace();
            System.exit(0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

